import '../../styles/App.css';
import React, {useState} from 'react';
import {TimePicker} from "antd";
import data from './data.json'
import dayjs from "dayjs";
const format = 'HH:mm';


const RegisterRestaurant = () => {
    const [listDay, setListDay] = useState(data);
    const toggleRuhetag = (day) => {
        let newState = listDay.map(obj => {
            if (obj.id === day.id) {
                return {...obj, ruhetag: !day.ruhetag, isDisable: day.ruhetag};
            }
            return obj;
        });
        setListDay(newState)

    };
    const toggleMittagspause = (day) => {
        let newState = listDay.map(obj => {
            if (obj.id === day.id) {
                return {...obj, mittagspause: !day.mittagspause};
            }
            return obj;
        });
        setListDay(newState)
    }
    return (
        <div>
            {listDay && listDay.map(day => (
                    <div className="py-6 border-b-2 flex items-center justify-center" key={day.id}>
                        <div onClick={() => toggleRuhetag(day)} className="inline-grid">
                            <b className="pb-2">{day.name}</b>
                            {day.ruhetag ? <button className="border p-2 rounded w-24">Ruhetag + </button> :
                                <button className="bg-lime-400 p-2 border rounded w-24">Ruhetag - </button>}

                        </div>

                        <b className="px-32 w-[500px]">{day.isshowTime ?
                            <TimePicker.RangePicker defaultValue={[dayjs("09:00", format), dayjs("19:00", format)]}
                                                    disabledTime={() => ({
                                                        disabledHours: () => [0,1,2,3,4,5,6,7,8,20,21,22,23,24]
                                                    })}
                                                    disabled={day.isDisable} format={format}/> : 'Geschlossen'}</b>
                        <div onClick={() => toggleMittagspause(day)}>
                            {day.mittagspause ?
                                <button className=" border rounded border-solid p-2" disabled={day.isDisable}>Mittagspause + </button> :
                                <button disabled={day.isDisable} className="border rounded bg-lime-400 border-solid p-2">
                                    <span className="underline">Mittagspause - </span> <br/> 12:00 -13:00</button>}
                        </div>
                    </div>
                )
            )}
        </div>

    )
}
RegisterRestaurant.propTypes = {}


export default RegisterRestaurant;
