import './styles/App.css';
import RegisterRestaurant from "./components/Restaurant/RegisterRestaurant";


function App() {
  return (
    <div className="App w-2/4 m-auto border shadow-lg shadow-indigo-200/80 mt-12 rounded-lg">
      <h1 className="text-2xl font-bold text-lime-700 pt-8">
        Offnungszeiten
      </h1>
        <RegisterRestaurant/>
    </div>
  );
}

export default App;
